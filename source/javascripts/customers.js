/* eslint-disable no-undef, no-new, no-eq-null, eqeqeq */

(function() {
  var ROW_SIZE = 4;
  var ROWS_PER_PAGE = 4;

  var sortPagination = function(total, page) {
    var items = [];
    if (page > 1) items.push({ title: '<<', where: 1 });
    if (page > 1) {
      items.push({ title: '<', where: page - 1 });
    }

    if (page > 6) items.push({ title: '...', separator: true });

    var start = Math.max(page - 4, 1);
    var end = Math.min(page + 4, total);

    for (var i = start; i <= end; i++) {
      var isActive = i === page;
      items.push({ title: i, active: isActive, where: i });
    }

    if (total - page > 4) items.push({ title: '...', separator: true });

    if (total - page >= 1) {
      items.push({ title: '>', where: page + 1 });
    }

    if (total - page >= 1) items.push({ title: '>>', where: total });

    return items;
  };

  this.CustomerListHandler = (function() {
    function CustomerListHandler() {
      this.$filters = $('select[name=filter-industry]');
      this.$orgList = $('.org-image', '.org-groups-container');
      this.$orgContainer = $('.org-groups-container');
      // $(this.$filters[0]).attr('checked', 'checked');

      // binds
      this.$filters.on('change', this.render.bind(this));
      this.render.call(this);
    }

    CustomerListHandler.prototype.filterData = function() {
      var $filterValue = $('select[name=filter-industry]').val();

      var filteredData = this.$orgList.filter(function() {
        if ($filterValue === 'any') {
          return true;
        } else if ($filterValue === $(this).data('industry-type')) {
          return true;
        }
        return false;
      });

      this.groupedFilteredData = [];

      while (filteredData.length > 0) {
        this.groupedFilteredData.push(filteredData.splice(0, ROW_SIZE));
      }
    };

    CustomerListHandler.prototype.render = function(e) {
      this.filterData.call(this);
      function renderData(pageNumber) {
        if (!pageNumber) {
          this.currentPage = 1;
          this.renderOrgList.call(this);
        } else {
          var newPageNumber;
          if (pageNumber.text().toLowerCase() === '>') {
            newPageNumber = this.currentPage += 1;
          } else if (pageNumber.text().toLowerCase() === '<') {
            newPageNumber = this.currentPage -= 1;
          } else if (pageNumber.text().toLowerCase().indexOf('<<') !== -1) {
            newPageNumber = this.currentPage = 1;
          } else if (pageNumber.text().toLowerCase().indexOf('>>') !== -1) {
            newPageNumber = this.getTotalPages();
          } else {
            newPageNumber = parseInt(pageNumber.text(), 10);
          }
          this.currentPage = newPageNumber;
          this.renderOrgList.call(this);
        }
      }
      if (e != null) {
        var $callingElement = $(e.currentTarget);
        if ( !$callingElement.is('select') ) {
          renderData.call(this, $callingElement);
        } else {
          renderData.call(this);
        }
      } else {
        renderData.call(this);
      }
    };

    CustomerListHandler.prototype.renderOrgList = function() {
      this.$orgContainer.empty();
      $('.pagination li span').off('click');
      document.querySelector('.pagination').innerHTML =
      sortPagination(this.getTotalPages(), this.currentPage)
        .map(function(e) {
          var activeElement;
          if (e.active) {
            activeElement = '<li seperator="' + e.seperator + '" class="active" disabled="' + e.disabled + '">'
                            + '<span>' + e.title + '</span>'
                            + '</li>';
          } else {
            activeElement = '<li seperator="' + e.seperator + '" disabled="' + e.disabled + '">'
                            + '<span>' + e.title + '</span>'
                            + '</li>';
          }
          return activeElement;
        }).join('');
      $('.pagination li:not(.active) span').on('click', this.render.bind(this));

      // The organization table
      for (var i = (this.currentPage - 1) * ROWS_PER_PAGE;
        i < (this.currentPage * ROWS_PER_PAGE); i += 1) {
        var $rowContainer = $('<div class="row"></div>');
        if (this.groupedFilteredData[i] != null) {
          // eslint-disable-next-line no-loop-func
          this.groupedFilteredData[i].forEach(function(el) {
            var $columnContainer = $('<div class="col-xs-3 col-sm-3"></div>');
            $(el).appendTo($columnContainer);
            $columnContainer.appendTo($rowContainer);
          }, this);
          $rowContainer.appendTo(this.$orgContainer);
        }
      }
    };

    CustomerListHandler.prototype.getTotalPages = function() {
      return Math.ceil(this.groupedFilteredData.length / ROWS_PER_PAGE);
    };

    return CustomerListHandler;
  })();

  new CustomerListHandler();
})();
